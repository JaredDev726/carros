package com.carros.carros.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carros.carros.model.Carro;

@Repository
public interface ICarroRepository extends JpaRepository<Carro, Integer> {

}
