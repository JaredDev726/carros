package com.carros.carros.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carros.carros.model.Carro;
import com.carros.carros.service.CarroService;

@RestController
@RequestMapping("/api/carros")
public class CarroController {
	
	@Autowired
	private CarroService carroService;
	
	@GetMapping
	public ResponseEntity<List<Carro>> findAll(){
		return ResponseEntity.ok(carroService.findAll());
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Carro> findById(@PathVariable("id") Integer idCarro){
		return carroService.findById(idCarro)
				.map(ResponseEntity::ok)
				.orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	@PostMapping
	public ResponseEntity<Carro> create(@RequestBody Carro carro){
		return new ResponseEntity<>(carroService.create(carro), HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<Carro> delete(@RequestBody Carro carro){
		return carroService.findById(carro.getIdCarro())
				.map(c -> ResponseEntity.ok(carroService.update(carro)))
				.orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Carro> delete(@PathVariable("id") Integer idCarro){
		return carroService.findById(idCarro)
				.map(c -> {
					carroService.delete(idCarro);
					return ResponseEntity.ok(c);
				})
				.orElseGet(() -> ResponseEntity.notFound().build());
	}
}
