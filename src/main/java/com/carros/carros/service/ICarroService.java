package com.carros.carros.service;

import java.util.List;
import java.util.Optional;

import com.carros.carros.model.Carro;

public interface ICarroService {
	
	List<Carro> findAll();
	
	Optional<Carro> findById(Integer id);
	
	Carro create(Carro carro);
	
	Carro update(Carro carro);
	
	void delete(Integer id);

}
