package com.carros.carros.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carros.carros.model.Carro;
import com.carros.carros.repository.ICarroRepository;

@Service
public class CarroService implements ICarroService {
	
	@Autowired
	private ICarroRepository carroRepo;

	@Override
	public List<Carro> findAll() {
		return carroRepo.findAll();
	}

	@Override
	public Optional<Carro> findById(Integer id) {
		return carroRepo.findById(id);
	}

	@Override
	public Carro create(Carro carro) {
		return carroRepo.save(carro);
	}

	@Override
	public Carro update(Carro carro) {
		return carroRepo.save(carro);
	}

	@Override
	public void delete(Integer id) {
		carroRepo.deleteById(id);
	}

}
